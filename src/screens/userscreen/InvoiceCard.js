import React, { Component } from "react";
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, } from "react-native";

import {db} from '../userscreen/config'
import ItemComponent from './FoodComponent3';
import { Button } from "native-base";

let itemsRef = db.ref('/Order')
let addItem = item => {  
  db.ref('/Order').push({
    name: item
  });
};

console.disableYellowBox = true;
class InvoiceCard extends Component {
  state = {
    items: []

  };


  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    header: null
  }
  componentDidMount() {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      let items = Object.values(data);
      this.setState({ items });
    });
  }

  render(props) {
    return (
      
      
        <View style={{ flex: 1,  }}>
           {this.state.items.length > 0 ? (
          <ItemComponent items={this.state.items} />
          
        ) : (
          <Text>No items</Text>
          
        )}
         
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1, margin: 5 }}>
  
            </View>
          </View>
        
          
          <View style={{ marginBottom: 5 }} />
          <View style={styles.seperateLine} />
        </View>
  
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ebebeb'
  },
  imgStyle: {
    height: 160,
    width: null,
    borderRadius: 5,
    overflow: "hidden",
    marginVertical: 5
  },
  head: {
    backgroundColor: "white",
    marginHorizontal: 5,
    marginTop: 5
  },
  seperateLine: {
    borderBottomColor: "#d3d3d3",
    borderBottomWidth: 0.5,
    marginTop: 5
  },
  placeTitle: {
    marginTop: 5,
    color: "red",
    fontSize: 18
  },
  placeDescription: {
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10,
    color: "grey",
    fontSize: 12
  },
  textHeader: {
    color: "grey",
    fontSize: 14
  },
  profile: {
    height: 40,
    width: 40,
    borderRadius: 40 / 2
  },
  nameText: {
    fontSize: 14,
    color: "red",
    fontWeight: "bold"
  }
});

export default InvoiceCard;
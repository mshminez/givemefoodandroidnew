//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import { StyleSheet, View, Text } from 'react-native';
// import all basic components
import Invoice from './InvoiceCard';
export default class InvoicePage extends Component {
  //Screen1 Component
  render() {
    return (
      <View style={styles.MainContainer}>
              <Invoice navigation= {this.props.navigation} />
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    paddingTop: 20,
  },
});
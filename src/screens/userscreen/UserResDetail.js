import React, { Component } from "react";
import { Text, View, StyleSheet, Image, TouchableOpacity, TouchableHighlight } from "react-native";
import { Icon, Header, Left, Right, Title, Body, Footer } from "native-base";
import FoodCard from '../FoodCard';
import { ScrollView } from "react-native-gesture-handler";



export default class UserResDetail extends Component {

    render(props) {
        return (
            <View style={styles.container}>

              <Text style={{ fontSize: 18, fontWeight: "500" }}>
              Relax house
              </Text>

                <Image source={{ uri: 'http://halalinthailand.com/wp-content/uploads/2018/02/IMG_0216-1024x682.jpg' }} style={styles.imagestyle} />



                <View style={styles.detailstyle}>


                    <Text>Detail :</Text>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon type="Entypo"  style={styles.textDetail} />
                        <Text style={styles.textDetail}>Open 09.00 - 21.00</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon type="Entypo"  style={styles.textDetail} />
                        <Text style={styles.textDetail}>islam food</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon type="Entypo" style={styles.textDetail} />
                        <Text style={styles.textDetail}></Text>
                    </View>


                </View>

                <Text style={styles.textDetail} >FOOD LIST</Text>

                <View style={styles.foodstyle}>

                    <ScrollView horizontal={true}>
                        <FoodCard navigation={this.props.navigation} />
                      
                    </ScrollView>
                </View>


            </View>
        
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFF3F1'

    },
    
    detailstyle: {
        width: '95%',
        height: '20%',
        margin: 10,
        backgroundColor: '#FFFFFF',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.3,
        shadowRadius: 3.5,
        elevation: 5,
        justifyContent: 'space-around',
        padding: 10,
        paddingLeft: 20,
        borderRadius: 5,
        borderWidth: 0.2,
        borderColor: '#FF9A55'
    },
    imagestyle: {
        width: '100%',
        height: '33%',
        backgroundColor: '#ffdeEE'

    },

    foodstyle: {
        width: '95%',
        height: '30%',
        margin: 5,
        backgroundColor: '#FFFFFF',
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'space-between',
        flexDirection: 'row',
        borderRadius: 5,
        borderWidth: 0.2,
        borderColor: '#FF9A55',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.3,
        shadowRadius: 3.5,
        elevation: 5,
        justifyContent: 'space-around'
    },

    foodcardstyle: {
        flexDirection: 'row',
    },

    textDetail: {
        fontSize: 15,
        color: '#2b2b2b'
    },

})
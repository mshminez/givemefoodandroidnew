import React, { Component } from "react";
import { Modal, Text, View, StyleSheet, Image, TouchableOpacity, TouchableHighlight, Alert,TextInput } from "react-native";
import { Icon, Header, Left, Right, Title, Body, Footer } from "native-base";
import { db } from './config';
import Foodcomponent from './FoodComponent';
let itemsRef = db.ref('/Foods')



export default class UserResFoodDetail extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        modalVisible: false,
        Destination: '',
        name: 'Bezel Chicken',
        price:45
       
        
      };
    addItem = () => {  
        db.ref('/Order').push({
          Destination:this.state.Destination,
          name: this.state.name,
          price:this.state.price
      
        });
      };
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    
  handleChange = e => {

    this.setState({
      [name]: e.nativeEvent.text,
    });
  };
  handleSubmit = () => {
    this.addItem(this.state.name),
    (this.onClickListener('sign_up')),

    (this.props.navigation.navigate('Invoice'));
 
   

  };
  componentDidMount() {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      let items = Object.values(data);
      this.setState({ items });
    });
  }

  onClickListener = (viewId) => {
    Alert.alert("","ORDER SUCCESS");
  }

    render(props) {
        return (
            <View style={styles.container}>

                <Image source={{ uri: 'https://i.ytimg.com/vi/Z6wYm_aS0Sk/maxresdefault.jpg' }} style={styles.imagestyle} />
                <View style={styles.detailstyle}>


                    <Text style={{ fontSize: 20 }}>Detail :</Text>
                    <Text style={{ fontSize: 20 }}>{this.state.name}</Text>
                    <Text style={{ fontSize: 20 }}>Price: {this.state.price}</Text>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                        <Text style={styles.textDetail}></Text>
                    </View>


                </View>

                <Footer style={styles.buttonStyle}>

                    <Modal animationType="slide" transparent={false} visible={this.state.modalVisible} >
                        <View style={styles.modalContainer}>
                            <View style={styles.innerContainer}>
                            <Image source={{ uri: 'http://www.pngmart.com/files/7/Delivery-PNG-Pic.png' }} style={{width: 300, height: 250}} />
                            <Text style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    marginTop: 40
                }}>Delivery Destination</Text>
                <TextInput
                    style={{
                        height: 40,
                        borderBottomColor: 'gray',
                        marginLeft: 30,
                        marginRight: 30,
                        marginTop: 10,
                        marginBottom: 20,
                        borderBottomWidth: 1
                    }}
                    
                    onChangeText={(text) => this.setState({ Destination: text })}
                    placeholder="Enter Destination And Phone Number"
                 
                />


                                <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]}  onPress={this.handleSubmit} >
                                    <Text style={styles.signUpText}>Confirm</Text>
                                </TouchableHighlight>

                            </View>
                        </View>

                    </Modal>

                    <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => { this.setModalVisible(true); }}>
                        <Text>Order</Text>
                    </TouchableHighlight>


                </Footer>


            </View>
        );
    }
}

const styles = StyleSheet.create({

   
    buttonStyle: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFF3F1',
        flexDirection: 'row',
        padding: 10,
        paddingLeft: 20,
        width: '100%',
        height: '15%',
        justifyContent: 'space-around',

    },

    buttonContainer: {
        height: 40,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: 150,
        borderRadius: 5,
    },

    signupButton: {
        backgroundColor: "#ffa211",
    },
    UserhomeButton: {
        backgroundColor: "#69D81A",
    },

    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
        
    },

    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#FFF3F1',
    },
    innerContainer: {
        justifyContent: 'center',
        alignItems: 'center',

    },

    detailstyle: {
        width: '98%',
        height: '45%',
        margin: 10,
        borderRadius: 5,
        borderWidth: 1.5,
        borderColor: '#FF9A55',
     
        justifyContent: 'space-around',
        justifyContent: 'flex-start',
        padding: 10,
        paddingLeft: 15
    },
    imagestyle: {
        width: '100%',
        height: '40%',
        backgroundColor: '#FFF3F1'

    },

    foodstyle: {
        width: '95%',
        height: '35%',
        margin: 10,
        backgroundColor: 'orange',
        padding: 10,
        paddingLeft: 20,
        padding: 10,
        paddingLeft: 20,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },

    foodcardstyle: {
        flexDirection: 'row',
    },

    textDetail: {
        fontSize: 15,
        color: '#2b2b2b'
    },

})
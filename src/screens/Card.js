import React, { Component } from "react";
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView } from "react-native";

import {db} from '../screens/userscreen/config'
import ItemComponent from './userscreen/FoodComponent';

let itemsRef = db.ref('/promotion')
let addItem = item => {  
  db.ref('/promotion').push({
    name: item
  });
};
console.disableYellowBox = true;
class Card extends Component {
  state = {
    items: []

  };



  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    header: null
  }
  componentDidMount() {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      let items = Object.values(data);
      this.setState({ items });
    });
  }

  render(props) {
    return (
      
      <TouchableOpacity style={{ flex: 1, backgroundColor: "#fff" ,borderRadius: 10,}} onPress={() => this.props.navigation.navigate('UserResDetail')}>
        <View style={{ flex: 1, marginHorizontal: 5, marginTop: 5 }}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1, margin: 5 }}>
              <Text style={{ fontSize: 18, fontWeight: "500" }}>
              Relax house
              </Text>
            </View>
          </View>
          <View>
            <Image source={{ uri: 'http://halalinthailand.com/wp-content/uploads/2018/02/IMG_0216-1024x682.jpg' }} style={styles.imgStyle} />
          </View>   
          <Text
            numberOfLines={2}
            style={{
              flex: 1,
              marginVertical: 5,
              fontSize: 17,
              fontWeight: "500"
            }}
          >
           PROMOTION
      
          </Text>
          <View style = {{ flex: 2}}>

        {this.state.items.length > 0 ? (
          <ItemComponent items={this.state.items} />
        ) : (
          <Text>No items</Text>
        )}
        </View>
     
          <View style={{ marginBottom: 5 }} />
          <View style={styles.seperateLine} />
        </View>
        
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ebebeb'
  },
  imgStyle: {
    height: 160,
    width: null,
    borderRadius: 5,
    overflow: "hidden",
    marginVertical: 5
  },
  head: {
    backgroundColor: "white",
    marginHorizontal: 5,
    marginTop: 5
  },
  seperateLine: {
    borderBottomColor: "#d3d3d3",
    borderBottomWidth: 0.5,
    marginTop: 5
  },
  placeTitle: {
    marginTop: 5,
    color: "red",
    fontSize: 18
  },
  placeDescription: {
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10,
    color: "grey",
    fontSize: 12
  },
  textHeader: {
    color: "grey",
    fontSize: 14
  },
  profile: {
    height: 40,
    width: 40,
    borderRadius: 40 / 2
  },
  nameText: {
    fontSize: 14,
    color: "red",
    fontWeight: "bold"
  }
});

export default Card;
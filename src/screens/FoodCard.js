import React, { Component } from "react";
import { Text, View, StyleSheet, Image, TouchableOpacity, Button,TouchableHighlight } from "react-native";
import { db } from './userscreen/config';
import ItemComponent from './userscreen/FoodComponent';

let itemsRef = db.ref('/Foods')
let addItem = item => {  
  db.ref('/Foods').push({
    name: item
  });
};
class FoodCard extends Component {
  state = {
    name: ''
  };
  state = {
    items: []
  };
  componentDidMount() {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      let items = Object.values(data);
      this.setState({ items });
    });
  }

  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    header: null
  }
 

  render(props) {
    return (

      <View>
        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.navigation.navigate('UserResFoodDetail')}>
          <View style={{ flex: 1, marginHorizontal: 2, marginTop: 2 }}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ flex: 1, margin: 2 }}>
              </View>
            </View>
            <View>
              <Image source={{ uri: 'https://i.ytimg.com/vi/Z6wYm_aS0Sk/maxresdefault.jpg' }} style={styles.imgStyle} />
            </View>
            <View style = {{ flex: 2}}>


  <ItemComponent items={this.state.items} />


</View>

            <View style={{ marginTop: 2 }}>
              <Text style={{ color: "#2b2b2b", lineHeight: 21 }}>
           
            </Text>
            </View>
            <View style={{ marginBottom: 2 }} />
          </View>
        </TouchableOpacity>

        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  
  imgStyle: {
    height: 100,
    width: 100,
    marginVertical: 2,
    marginRight : 10,
    marginTop : 5
  },
  head: {
    backgroundColor: "white",
    marginHorizontal: 5,
    marginTop: 5
  },
  seperateLine: {
    borderBottomColor: "#d3d3d3",
    borderBottomWidth: 0.5,
    marginTop: 5
  },
  
  placeTitle: {
    marginTop: 5,
    color: "red",
    fontSize: 18
  },
  placeDescription: {
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
    color: "grey",
    fontSize: 12
  },
  textHeader: {
    color: "grey",
    fontSize: 14
  },
  profile: {
    height: '10%',
    width: '15%',

  },
  nameText: {
    fontSize: 14,
    color: "red",
    fontWeight: "bold"
  }
});

export default FoodCard;

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Platform,
  ScrollView,
  Picker,
  Button,
  Alert,
  Image,
  ActivityIndicator
} from 'react-native'
import ImagePicker from 'react-native-image-picker'
import RNFetchBlob from 'react-native-fetch-blob'
import {storage} from '../userscreen/config'

console.disableYellowBox = true;
// Prepare Blob support
const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob
const uploadImage = (uri, mime = 'application/octet-stream') => {
  return new Promise((resolve, reject) => {
    const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
      const sessionId = new Date().getTime()
      let uploadBlob = null
      const imageRef = storage.ref('images').child(`${sessionId}`)

      fs.readFile(uploadUri, 'base64')
      .then((data) => {
        return Blob.build(data, { type: `${mime};BASE64` })
      })
      .then((blob) => {
        uploadBlob = blob
        return imageRef.put(blob, { contentType: mime })
      })
      .then(() => {
        uploadBlob.close()
        return imageRef.getDownloadURL()
      })
      .then((url) => {
        resolve(url)
      })
      .catch((error) => {
        reject(error)
      })
  })
}


 
export default class Screen2 extends Component {
  constructor(){
		super();
		this.state={
			PickerValue:''
			
		}
		
  };
  clickme=()=>{
		var data = this.state.PickerValue;
		if(data==""){
			alert("Please Select a Option");
		}else{
			alert(data);
		}
		
	}
 
  getSelectedPickerValue=()=>{
     Alert.alert("Selected country is : " +this.state.PickerSelectedVal);
   }


  _pickImage() {
    this.setState({ uploadURL: '' })

    ImagePicker.launchImageLibrary({}, response  => {
      uploadImage(response.uri)
        .then(url => this.setState({ uploadURL: url }))
        .catch(error => console.log(error))
    })
  }


  render() {
    const { photo } = this.state;
    return (
      <View style={{ flex: 1}}>
      <ScrollView>
      {
        (() => {
          switch (this.state.uploadURL) {
            case null:
              return null
            case '':
              return <ActivityIndicator />
            default:
              return (
                <View>
                  <Image
                    source={{ uri: this.state.uploadURL }}
                    style={ styles.image }
                  />
             
                </View>
              )
          }
        })()
      }
      <TouchableOpacity onPress={ () => this._pickImage() }>
        <Text style={ styles.upload }>
          Upload Image
        </Text>
      </TouchableOpacity>
      <View style ={{flex:2}}>
      <View>
                </View>

                <Text style={styles.title}>Add food</Text>
                <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://img.icons8.com/color/40/000000/thanksgiving.png'}}/>
          <TextInput style={styles.inputs}
              placeholder="Food name"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(fullName) => this.setState({fullName})}/>
        </View>
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://img.icons8.com/dusk/40/000000/price-tag-euro.png'}}/>
          <TextInput style={styles.inputs}
              placeholder="Price"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(fullName) => this.setState({fullName})}/>
        </View>
       
        <View >
        <Text style={ styles.ss1 }>
         Select ingredients
        </Text>
		<Picker
		style={{width:'80%'}}
		selectedValue={this.state.PickerValue}
		onValueChange={(itemValue,itemIndex) => this.setState({PickerValue:itemValue})}
		>
		<Picker.Item label="pork" value="html" />
		<Picker.Item label="chicken" value="javascript"/>
    <Picker.Item label="meat" value="javascript"/>
		</Picker>    
    
      </View>
      <TouchableOpacity onPress={ () => this._pickImage() }>
        <Text style={ styles.upload }>
          Upload Foodmenu
        </Text>
      </TouchableOpacity>
      </View>
      
      </ScrollView>
    </View>
  )
}
}

const styles = StyleSheet.create({
container: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#F5FCFF',
},
title: {
  marginBottom: 20,
  fontSize: 25,
  textAlign: 'center'
},
itemInput: {
  height: 50,
  padding: 4,
  marginRight: 5,
  fontSize: 23,
  borderWidth: 1,
  borderColor: '#F98742',
  borderRadius: 8,
  color: '#8B8B8B'
},
container: {
  flex: 2,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#F5FCFF',
},
image: {
  width: 600, height: 200,

},
inputContainer: {
  borderBottomColor: '#F5FCFF',
  backgroundColor: '#FFFFFF',
  borderRadius:30,
  borderBottomWidth: 1,
  width:250,
  height:45,
  marginBottom:20,
  flexDirection: 'row',
  alignItems:'center'
},
inputIcon:{
  width:30,
  height:30,
  marginLeft:15,
  justifyContent: 'center'
},
ss1: {
  textAlign: 'center',
},
upload: {
  textAlign: 'center',
  color: '#F98742',
  padding: 10,
  marginBottom: 5,
  borderWidth: 1,
  borderColor: '#F98742'
  
},
})
